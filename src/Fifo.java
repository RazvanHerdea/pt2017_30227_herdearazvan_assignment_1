import java.awt.List;
import java.util.LinkedList;

public class Fifo {
    //public class Fifo1<String>{
    public LinkedList<Client> list = new LinkedList<>();

    public int sizeMax;
    public int nrClienti=0;

    public void Fifo( int sizeMax,int nrClienti){
        this.sizeMax = sizeMax;
        this.nrClienti = nrClienti;
    }
    public void adauga(Client cl){
        nrClienti++;
        list.add(cl);
        System.out.println("a intrat clientul "+cl.nume);
        System.out.println("Coada are:"+nrClienti+" clienti");
    }

    public void scoate(Client cl){
        nrClienti--;
        list.remove(cl);
        System.out.println("a iesit "+cl.nume);
        System.out.println("Coada are:"+nrClienti+" clienti");

    }

    public int listSize(){
        return list.size();
    }

    public String show(){
        String s="";

        for(Client cl: list){
            s = s+ cl.toString();
        }
        return s;
    }

}




