import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View extends JFrame{
    public JFrame fereastra = new JFrame();
    public JLabel Casa1 = new JLabel("Casa1:");
    public JLabel Casa2 = new JLabel("Casa2:");
    public JLabel avgServL = new JLabel("AVG Service:");
    public JLabel avgEmpty = new JLabel("empty queue:");
    public JLabel avgWait = new JLabel("AVG Wait:");
    public JButton start = new JButton("START");
    public JLabel showCounter = new JLabel("Time");
    public JLabel showSec = new JLabel("Secunda cu cei mai multi clienti!");

    public JLabel banda1 = new JLabel("  ");
    public JLabel banda2 = new JLabel("   ");

    public JTextField avgService= new JTextField();
    public JTextField emptyQ= new JTextField();
    public JTextField avgW= new JTextField();




    public View(){
        fereastra.setSize(600,600);
        fereastra.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Casa1.setBounds(70,60,70,80);
        fereastra.add(Casa1);

        Casa2.setBounds(70,150,70,80);
        fereastra.add(Casa2);

        banda1.setBounds(110,60,300,80);
        fereastra.add(banda1);

        banda2.setBounds(110,150,300,80);
        fereastra.add(banda2);

        avgService.setBounds(125,220,50,30);
        fereastra.add(avgService);

        emptyQ.setBounds(125,245,50,30);
        fereastra.add(emptyQ);


        avgServL.setBounds(30,220,80,30);
        fereastra.add(avgServL);

        avgEmpty.setBounds(30,245,80,30);
        fereastra.add(avgEmpty);

        avgWait.setBounds(30,280,80,30);
        fereastra.add(avgWait);

        avgW.setBounds(125,280,50,30);
        fereastra.add(avgW);

        start.setBounds(200,10,120,50);
        fereastra.add(start);


        showCounter.setBounds(300,400,300,30);
        fereastra.add(showCounter);

        showSec.setBounds(30,300,190,35);
        fereastra.add(showSec);



        fereastra.setLayout(null);
        fereastra.setVisible(true);


    }



    public void attachStart(ActionListener a) {
        start.addActionListener(a);
    }
    public void setAVGService(int g){avgService.setText(""+g);}
    public void setEmptyQ(int g1){emptyQ.setText(""+g1);}
    public void setavgWating(int x3){avgW.setText(""+x3);}
    public void setCounter(int counter){
        showCounter.setText(""+counter);
    }
    public void setBanda1(String s){
        banda1.setText(s);
    }
    public void showS(int e){showSec.setText(" "+e);}
    public void setBanda2(String s){
        banda2.setText(s);
    }

}
